package HomeWork11;

//Make a Logger class with a void log (String message) method that prints any message to the console.
class Logger {
    private static final Logger logger;

     private Logger() {}

    //Apply the Singleton pattern for the Logger.
    static {
        logger = new Logger();
    }

    public static Logger getLogger() {
        return logger;
    }

    public void log(String message) {
        System.out.println(message);
    }
}
