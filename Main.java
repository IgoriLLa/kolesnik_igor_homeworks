package HomeWork08;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Human[] humans = new Human[10];
        Scanner scannerInputName = new Scanner(System.in);
        Scanner scannerInputWeight = new Scanner(System.in);

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();

            System.out.println("Enter name of " + (i + 1) + " human:");
            String nameHuman = scannerInputName.next();
            humans[i].setName(nameHuman);

            System.out.println("Enter weight in kg. of " + (i + 1) + " human:");
            double weightHuman = scannerInputWeight.nextDouble();

            humans[i].setWeight(weightHuman);
        }

        sortByWeight(humans);

        for (Human human : humans) {
            System.out.print(" вес " + human.getName() + " равен " + human.getWeight());
        }
    }

    public static void sortByWeight(Human[] humans) {
        for (int i = 0; i < humans.length - 1; i++) {
            int minIndex = i;
            for (int j = i; j < humans.length; j++) {
                if (humans[j].getWeight() < humans[minIndex].getWeight()) {
                    minIndex = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minIndex];
            humans[minIndex] = temp;
        }
    }
}
