package HomeWork07;

import java.util.Scanner;

/**
 * The input is a sequence of numbers ending in -1.
 * It is necessary to print the number that appears in the sequence the minimum number of times.
 * Guaranteed:
 * All numbers in the range -100 to 100.
 * Numbers occur no more than 2,147,483,647 times each.
 * Algorithm complexity - O (n)
 */

public class HomeWork07 {

    public static void main(String[] args) {
        System.out.println("Please enter a number between -100 and 100:");
        Scanner scanner = new Scanner(System.in);

        int[] subsequenceArray = new int[201];
        int meetIntNumberMax = Integer.MAX_VALUE;
        int minValue = 0;
        int result = 0;

        while (scanner.hasNext()) {
            int intNumber = scanner.nextInt();
            if (intNumber == -1) {
                break;
            } else if (intNumber >= -100 && intNumber <= 100 && subsequenceArray[intNumber + 100] < meetIntNumberMax) {
                subsequenceArray[intNumber + 100]++;
            } else {
                System.out.println("Wrong number, number must be in the range from -100 to 100:");
            }
        }

        for (int i = 0; i < subsequenceArray.length; i++) {
            if (minValue == 0 && subsequenceArray[i] != 0 ||
                    subsequenceArray[i] > 0 && subsequenceArray[i] < minValue) {
                minValue = subsequenceArray[i];
                result = i - 100;
            }
        }
        System.out.println("The number that appears in the sequence the minValue number of times: " + result);
    }
}

